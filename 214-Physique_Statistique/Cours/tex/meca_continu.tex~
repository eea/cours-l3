\documentclass[../main.tex]{subfiles}
\begin{document}

\section{Dénombrements des états}

\subsection{Mécanique Hamiltonienne}

Considérons un système composé de N particules. L'iddée de la mécanique Hamiltonnienne est de donner une nouvelle formulation de la mécanique classique en ayant pour objectif d'en simplifier les équations. 
On introduit deux nouvelles variables:

\begin{itemize}
	\item $p_i$: Moment conjugué ou impulsion généralisée (c'est une quantité de mouvement)
	\item $q_i$: Position
\end{itemize}

En considérant les particules dans l'espace, on posera par la suite: 
\[ p = (\vec{p_1}, ..., \vec{p_N}) \text{ et de même } q = (\vec{q_1}, ..., \vec{q_N}) \]
Il apparait que l'on peut décrire l'état du système dans l'espace $(p, q)$. Ce nouvel espace à \textbf{6N coordonnées} est appelé \textbf{espace des phases}. Le lieu de cet espace est appelé \textbf{orbite}.\\
On peut introduire l'expression du volume élémentaire de l'espace des phases en cartésien : 

\[ \boxed{dpdq = \prod_{i = 1}^N dp_{ix}dp_{iy}dp_{iz}dq_{ix}dq_{iy}dq_{iz} }  \]

En effectuant l'hypothèse que l'énergie du système s'écrit sous la forme: 

\[ H = \sum_{i = 1}^N H(\vec{p_i}, \vec{q_i}) + \underbrace{H_{interaction}}_{\text{négligeable}} \approx \sum_{i = 1}^N H(\vec{p_i}, \vec{q_i})  \]

On a l'expression des équations hamiltonniennes: 

\begin{equation*}
	\boxed{
		\boxed{
			\begin{syslin}
				\dot{p_i} = - \derivp[H]{q_i} \\
				\dot{q_i} = \derivp[H]{p_i}
			\end{syslin}
		}
	}
\end{equation*}

\textbf{Exemple: } 1 Particule

l'expression de l'Hamiltonien ($\equiv$ énergie) d'une particule est: 

\[ H(\vec{p}, \vec{q}) = \frac{p^2}{2m} + \underbrace{V(q)}_{\text{Potentiel}} \]

Puisque $V$ est un potentiel, \[ \derivp[V]{q} = - F_{ext} \]
Les équations hamiltonniennes donnent: 

\[
\begin{syslin}
	-\dot{p} = \derivp[V]{q} = -F_{ext} \\
	\dot{q} = \frac{p}{m}
\end{syslin}
\Rightarrow m\ddot{q} = F_{ext} \text{ (PFD 1D pour une particule)}
 \]

\subsection{expression de la densité de probabilité dans l'espace des phases}

En raisonnant dans l'espace des phases, nous notons $\rho (p,q) $ la densité d'état. Ainsi; 

\[ \boxed{\rho (p,q) dpdq = \text{Nombre d'états ( $\mu$-états) ayant  comme phase p et q à dpdq près} }\]
 
 On peut donc définir $\rho (E)$ telle que $\rho (E)dE$ soit le nombre de $\mu$-états d'énergie E à dE près. De plus, 
 
 \[ dn = \rho(p,q) dpdq \]
 Donc
 \[ \int_{\text{Vol. access.}} \rho(p,q) dpdq = N \]
 
 En posant alors 
 \[ \omega (p,q) = \frac{\rho(p,q)}{N}  \]
 
 Il vient:
 
 \[ \boxed{\boxed{ \int_{\text{Vol. access.}} \omega(p,q) dpdq = 1}}  \]
 
 $\omega(p,q)$ est donc la densité de probabilité dans l'espace des phases.
 
 \subsection{Théorème de Liouville}
 
 \textbf{Enoncé: } Un flot hamiltonien conserve le volume dans l'espace des phases.
 \\
 
 \textbf{Conséquence: }
 
 La conservation du volume donne, pour notre densité de probabilité: 
 
 \[ \deriv[\rho]{t} = 0 = \derivp[\rho]{t} + \rho div \vec{v} \]
 
 Donc \[ \derivp[\rho]{t} + \rho (\derivp[\dot{p}]{p} + \derivp[\dot{q}]{q} ) = 0  \]
Or, puisque le flot est hamiltonien 

\[ \begin{syslin}
	\dot{p} = -\derivp[H]{q} \\
	\dot{q} = \derivp[H]{p}
\end{syslin} \]

d'où \[ \derivp[\rho]{t} - \rho \frac{\partial^2H}{\partial{p}\partial{q}} + \rho \frac{\partial^2H}{\partial{p}\partial{q}} = 0 \]
donc \[ \boxed{ \derivp[\rho]{t} = 0 } \]
La loi de densité dans l'espace des phases ne dépend pas explicitement du temps.

\subsection{Approximations continues}

D'un point de vue quantique, \[ \epsilon_{\lambda} = |E_{l+1} - E_l | << \delta E << E \]

Donc \[ dn(E) = \rho (E)dE \]
En posant $ \omega (E)$ la densité de probabilité associée, on obtient:

\[ \boxed{ < \mathcal{O(E)} > = \int_{\text{E. acc.}} \omega (E) \mathcal{O}(E)dE } \]

$\mathcal{O(E)}$ est un \emph{observable} ($\equiv $Energie, Vitesse, ...)
\subsubsection{Comment définir le nombre de $\mu$-états ?}
En mécanique quantique, un $\mu$-états occupe un volume élémentaire dans l'espace des phases. 

\[ N = \frac{1}{\text{Vol. él.}} \int_{Vol. acc.} dpdq  \]

Or par De Broglie:

\[ \vec{p} = \hbar \vec{k} \text{ donc } \begin{syslin}
k_x = \frac{2\pi}{\lambda_x}\\
k_y = \frac{2\pi}{\lambda_y}\\
k_z = \frac{2\pi}{\lambda_z}
\end{syslin}  \]

\[ \lambda_x = \frac{L_x}{n_x} \text{ et }\lambda_y = \frac{L_y}{n_y} \text{ et }\lambda_z = \frac{L_z}{n_z} \]
i.e. \[ \vec{p} = h(\frac{n_x}{L_x}\vec{x}  + \frac{n_y}{L_y}\vec{y}  + \frac{n_z}{L_z}\vec{z}  )  \]
Puisque nous considérons un volume élémentaire, $\forall i \in [| 1,3|] n_i = 1$
\\


Pour une particule, le volume occupée est de $ \frac{h^3}{L_xL_yL_z} V = h^3$

\section{gaz monoatomique classique en situation $\mu$-canonique}

On considère un système de volume $\mathcal{V}$ d'énergie E fixée à $\delta E$ près. 

\[ H(p_i, q_i) = \frac{p^2}{2m} + V(q) \text{ avec } \begin{syslin}
V(q) = 0 \text{ si v } \in \mathcal{V} \\
V(q) = +\infty \text{ si v }\notin \mathcal{V}
\end{syslin}
\]
En $\mu$-canonique, la loi de densité de probabilité est uniforme, donc $\omega(p,q) = cste$.

Pour les particules dans $\mathcal{V}$:

\[ H = \sum_{i =1}^N \frac{p_i^2}{2m} \]

A l'équilibre, $S^* = K_B ln(\Omega^*)$ avec $\Omega^*$ le nombre de $\mu$-états en situation $\mu$-canonique.

\[ \boxed{ \Omega_{\text{indisc.}}^* = \frac{1}{N!(h^3)^N}\int_{E\leq H(p,q) \leq E + \delta E} dpdq } \]

\textbf{Remarque: } Le "$N!$" est présent car les particules sont indiscernables.
\\

Dans ce cas précis, H ne dépendant que de p,

\[ \Omega_{\text{indisc.}}^* =  \frac{V^N}{N!(h^3)^N}\int_{E\leq H(p,q) \leq E + \delta E} dp  \]

\textbf{proposition: Volume d'une boule de dimension q}

On définit une boule de dimension q de rayon $\sqrt{E}$ par la relation:  

\[ \sum_{i = 1}^q x_i^2 \leq E \]

Alors, notons $\Phi(E)$ le volume d'une telle boule, 

\[ \boxed{ \Phi(E) = \frac{\pi^{\frac{3N}{2}} \sqrt{E}^{3N}}{\Gamma(\frac{3N}{2} + 1)} \underset{N \rightarrow + \infty}{\sim} \frac{  \pi^{\frac{3N}{2}} \sqrt{E}^{3N} }{(\frac{3N}{2}!)}} \]

En revenant à notre cas, ayant $x_i = \frac{\sqrt{p_i}}{2m}$ alors $dp = \sqrt{2m}dx$

\[  \Omega_{\text{indisc.}}^* =  \frac{V^N(\sqrt{2m})^{3N}}{N!(h^3)^N} \underset{ E \leq \sum_{i = 1}^N x_i^2 \leq E+\delta E}{\int} (\prod_{i = 1}^N dx_i) \]
i.e.
\[ \Omega_{\text{indisc.}}^*  = \frac{V^N (2m)^{\frac{3N}{2}}}{N!h^{3N}}\underbrace{ [\Phi_{3N}(E+dE) - \Phi_{3N}(E) ] }_{ \underset{dE \rightarrow 0}{\sim} \Phi'(E)dE }  \]
donc
\[  \Omega_{\text{indisc.}}^* = \frac{(2m\pi E)^{\frac{3N}{2}}}{N!(\frac{3N}{2}!)}\frac{V}{h^3}^NdE \]

En composant par ln et en considérant l'approximation de Stirling, 

\[ \boxed{S = K_Bln(\Omega_{\text{indisc.}}^*)=K_BN[ln(\frac{V}{N}) + \frac{3}{2}ln(\frac{mE}{3\pi \hbar^2N}) + \frac{5}{2} ]} \text{ /!$\setminus$ fle à checker} \]

\textbf{Remarques: }



\begin{itemize}
	\item[$\bullet$] 
	\[ \frac{1}{T^*} = \derivp[S]{E} = \frac{3NK_B}{2E} \text{ donc } \boxed{E = \frac{3N}{2}K_BT} \]
	Chacunes des particules ont la même contribution à l'énergie: $\frac{K_BT}{2}$ (énergie statistique cinétique)
	\item[$\bullet$] Pour un gaz, $\delta W = -pdV = dE - TdS$ 
	donc
	\[ -pdV = dE - T(\derivp[S]{E}dE + \derivp[S]{V}dV) \]
	ainsi
	\[ -pdV = dE - dE -T\derivp[S]{V}dV \]
	i.e.
	\[ p = \frac{NK_BT}{V} \text{ on retrouve la loi des gaz parfaits} \]
	\item[$\bullet$] Ici, nous avons néglilgé les énergies d'intéractions. Lorsque la pression est trop grande, ces énergies ne sont plus négligeables( la loi des gaz parfait n'est vérifiée qu'à basse pression). 
	\item[$\bullet$] Pousser le raisonnement à prendre en compte les énergies d'intéractions permettrait de proposer un modèle thermodynamique des changements d'états.
\end{itemize}

\section{Loi des gaz parfaits en situation canonique}
En situation canonique, $\omega^c(p,q) \propto e^{-\beta H(p,q)}$ (A l'équilibre, on minimise $F = -K_BTlnZ$)

Volume accessible: Volume énergie$ [0, E_{tot}]$

\end{document}