# Prise de note des cours de SAPHIRE

Dans ce dépot, l'ensemble de ma prise de note des cours de SAPHIRE et même un peu plus.
Sont disponible:
 - 101 Maths (partie Distribution)
 - 105 Méthode Numériques
 - 106 Traitement du Signal
 - 211 Optimisation paramétrique
 - 214 Physique Statistique
 - 215 Probabilité
 - 232 Electronique Analogique
 - 233 Traitement de l'énergie
 - 235 Electromagnétisme
 
L'origine du travail provient du Dropbox/Drive du département EEA

Générer les pdf :
 1) Pour récuper les pdf s'assurer d'avoir `latexmk` d'installé et tout les packages utiles(dans le doute `sudo apt-intall texlive-full latexmk python3-pygments`)
 2) exécuter `make all -j<nombre de coeur de votre machine>` à la racine du dépot
 3) take a coffee

Télécharger la dernière mise à jour: [ici](https://perso.crans.org/comby/l3-eea)


Ne pas oublier de visiter la [page wiki du département](https://wiki.crans.org/VieEns/LesDépartements/DépartementEea)

