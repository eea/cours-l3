\documentclass[../232.tex]{subfiles}
\begin{document}
\section{L'amplificateur differentiel} 
	\subsection{Généralités}
	Dans de nombreux cas en électronique il est nécessaire d’amplifier la différence entre deux signaux noyés dans un bruit commun. On peut citer comme exemple les capteurs de température, de pression ou les jauges de déformation montés dans un pont de Wheatstone (qui joue le rôle de conditionneur de signaux). La sortie d’un amplificateur de différence (ou différentiel) doit restituer une image amplifiée de la différence des signaux d’entrée (souvent faibles niveaux), en rejetant l’influence du bruit commun (du même ordre de grandeur, voir très supérieur au signal utile). Cette sortie peut elle-même être différentielle, ou bien référencée à la masse. Il est important de noter que, comme dans les
	exemples cités, la grandeur dont l’image électrique est à amplifier peut être lentement variable (température ...). L’amplificateur différentiel devra donc être à liaison continue. De manière générale, la tension de sortie d’un amplificateur différentiel peut s’écrire comme :
	
	\[v_0= A_{d}(v_+-v_-)+ A_c\left(\frac{v_++v_-}{2}\right)\]
	 avec $(v_+-v_-)$ la tension différentielle à amplifier $A_d$ le gain de mode différentiel, $\left(\frac{v_++v_-}{2}\right)$ la composante parasite de mode commun,et $A_c$ le gain de mode commun. Pour un amplificateur différentiel idéal cette amplification de mode commun tend vers zéro.
	 
	 La capacité d’un amplificateur différentiel à rejeter correctement la composante de mode commun est
	 quantifiée par son taux de réjection de mode commun (TRMC, en anglais Common Mode Rejection
	 Ratio CMRR), défini comme le rapport de l’amplification différentielle (désirée élevée) à
	 l’amplification de mode commun (désirée nulle) : $TRMC = \left|\frac{A_d}{A_c}\right|$ ou bien exprimé en décibel :
	 $TRMC_{dB} = 20\log \left|\frac{A_d}{A_c}\right|$
	 
	
	\subsection{Etude de la paire différentielle}
	La brique de base de l’amplificateur différentiel est le circuit suivant, appelé paire différentielle. On
	retrouve ce montage utilisant des transistors bipolaires ou à effet de champ. L’étude de son
	fonctionnement est très similaire dans les deux cas.
	

	\begin{figure}[H]
		\centering
		\begin{circuitikz}
			\draw (-1,0) to[R,l=$R_0$,color=gray] ++(0,2)-- ++(-.5,0) to[Tnmos,l=$(1)$] ++(0,2) to[R,l=$R_{d1}$]++(0,1.5);
			\draw (1,0) to[I,i<=$i_0$] ++(0,2)-- ++(.5,0) to[Tnmos,l=$(2)$,mirror] ++(0,2) to[R,l=$R_{d2}$]++(0,1.5) -- ++(-3,0) node[midway,above]{+$V_{cc}$}; 
			\draw (-1.2,0)--(1.2,0) node[midway,below]{-$V_{cc}$} ;
		\end{circuitikz}
		\caption{Schéma de la paire différentielle}
	\end{figure}
	
	
		\subsubsection{Etude en petit signaux}
	Le schéma équivalent petits signaux du circuit est représenté ci-dessous. Dans un premier temps on
	supposera les résistances de drain égales, et les transistors parfaitement appairés.
	
	\begin{figure}
		\begin{subfigure}{.5\textwidth}
			\centering	
			\begin{circuitikz}
				\draw (0,0) node[ground]{};
				\draw (0,0) -- (-2,0) to[R,-|] ++(0,2) to[I,i=$g_mv_{g_1s}$] ++(0,2); 
				\draw (0,0) to[R,l=$R_0$,color=gray] ++(0,4);
				\draw (0,0) -- (2,0) to[R,-*] ++(0,2)node[right]{$V_0$} to[I,i_=$g_mv_{g_2s}$] ++(0,2); 
				\draw (-2,4)--(2,4) node[midway,above]{S};
			\end{circuitikz}
			\caption{Equivalent de la paire \\
				différentielle en petit signaux}
		\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
				Loi des noeuds en S : 
			\[ g_mv_{G1S}+g_mv_{g2s}=\frac{v_s}{R_0} = g_m(v_+-v_s)+g_m(v_--v_s)\]
			Donc:
			\[
			v_s = \frac{v_+ + v_-}{2+\frac{1}{g_mR_0}}
			\]
			De plus on a: \\
			$v_0= -g_mv_{g2s}R_D$ avec $V_{g2s}=v_--v_s$\\
			Donc:\\
			 \[
			v_0 = \frac{g_mR_D}{2}(v_+-v_-)-\frac{R_D}{2R_0+\frac{1}{g_m}}\left(\frac{v_++v_-}{2}\right)
			\]
		\end{subfigure}
		
	\end{figure}
	

	Ces résultats méritent plusieurs remarques. Concernant la tension de sortie : elle se compose de la somme d’un terme du au mode différentiel, de gain positif, et d’un terme du au mode commun négatif.
	Cette contribution de mode commun est d’autant plus réduite que R 0 est élevé, ce qui traduit la bonne qualité de la source de courant constant $I_0$. On retiendra donc que l’on a tout intérêt à polariser un montage à paire différentiel par une (bonne) source de courant. Cette remarque se retrouve au niveau du taux de réjection de mode commun, qui évolue comme $R_0$, et tend vers l’infini dans le cas d’une source de courant parfaite. On remarque également que le gain différentiel du circuit varie comme la résistance de drain. En pratique, pour augmenter ce gain, des charges actives remplacent les résistances de drain.
		\subsubsection{Etude en grand signaux}
		
	Afin d’étudier les limites de fonctionnement dans le régime linéaire des petits signaux de la paire
	différentielle, une étude en grands signaux est nécessaire. Pour cette étude les grandeurs courants
	tensions seront notées en majuscule, l’indice (t) étant supprimé afin alléger l’écriture, pour les
	distinguer du régime variationel petits signaux. Il ne faut pas pour autant les confondre avec les
	grandeurs de repos (DC).
	Première remarque : considérons une variation $\Delta I$ du courant de drain $I_{D1}$ . On a alors :$
		I_{D1}=\frac{I_0}{2}+\Delta I \text{ et } I_{D2}= \frac{I_0}{2}-\Delta I $
	De plus on a :
	\[
	 I_{D,i} =I_{DSS}(1-\frac{V_{gs,i}}{V_t})^2
	 \]
	d'où :
	\[
	 V_{G1S} = V_T \left(1-\sqrt{\frac{I_0+\Delta I}{I_{DSS}}}\right) 
	 \text{ et } 
	 V_{G2S} = V_T \left(1-\sqrt{\frac{I_0-\Delta I}{I_{DSS}}}\right)
	\]
	
	On note alors : $V_{diff}= V_{G1S}-V_{G2S}$ et donc
	
	\[ 
		\Delta I =\frac{\sqrt{I_D I_{DSS}}}{V_T}V_{diff}\sqrt{1-\frac{I_{DSS}}{4I_0V_T^2}V_{diff}^2}=G_mV_{diff}\sqrt{1-aV_{diff}^2}
	\]
	
	La tension de la paire différentielle s'écrit alors:
	$V_0=V_{cc}-R_D I_{D2} = V_{cc}-R_D\left(\frac{I_0}{2}-\Delta I\right)$ et si le courant de polarisation et la resistance de drain on été choisis tel qu'en l'absence de tension différentielle d'entrée ( ce qui impose $\Delta I = 0$ ) la tension de sortie soit nulle on a:
	\[
		V_0 = R_D G_m V_{diff}\sqrt{1-a V_{diff}^2}
	\]
	
	Le comportement en grand signaux de la paire différentielle n'est donc pas linéaire (ce qui étais prévisible). Cherchons la limite pour laquelle l'erreur due à la non linéarité soit inférieur à 1\%. On utilise l'approximation $\sqrt{1-x} \underset{x \ll 1}{\simeq}1-\frac{x}{2}$
	
	Alors $V_ = R_D G_m V_{diff} (1-\frac{a}{2} V_{diff}^2)$ et donc on veux vérifier
	 $\frac{-\sqrt{2}}{10\sqrt{a}}<V_{diff}<\frac{\sqrt{2}}{10\sqrt{a}}$ 
	Pour $V_{cc}=15V, I_0=1mA, R_D=30k\Omega, V_T=-2V$ on a $-0,4V< V_{diff} < 0,4V$

\begin{figure}{H}
	\centering
	\includegraphics[width=0.4\linewidth]{../img/ampli_diff}
	\caption{Caractéristique modèle petit et grand signaux}
	\label{fig:amplidiff}
\end{figure}


	
	
		
\end{document}